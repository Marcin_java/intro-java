//  To jest komentarz - to jest program Hello world
//To jest moj pierwszy pr


import zajecia6.MethodOverloadingExamples;
import zajecia6.Person;

/*
To jest
komentarz na wiele
lini kodu
To jest moj pierwszy pr
*/
public class Program {

    public static void main(String[] args) {
        MethodOverloadingExamples ex = new MethodOverloadingExamples();

        ex.add(2, 2);
        ex.add(2.0, 2.0);
        ex.add(2, 2.0);
        ex.add(2.0, 2);
        System.out.println('2');
    }
}

