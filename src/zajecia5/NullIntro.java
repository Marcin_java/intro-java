package zajecia5;

public class NullIntro {
    public static void main(String[] args) {
        String imie = "Piotr";
        System.out.println(imie.toUpperCase());

        imie = null;

        System.out.println(imie.toUpperCase());
    }
}
