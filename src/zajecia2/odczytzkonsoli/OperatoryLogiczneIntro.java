package zajecia2.odczytzkonsoli;

import java.util.Scanner;

/* Program pobierajacy dwie liczby calkowite i obliczajacy wyrazenie logiczne
    (a>=b)&&(a!=b)
 */
public class OperatoryLogiczneIntro {
    public static void main(String[] args) {
        int a;
        int b;

        //inicjalizacja obiektu typu Scanner
        Scanner odczyt = new Scanner(System.in);

        System.out.println("Podaj wartosc wyrazenia a");
        a = odczyt.nextInt();
        System.out.println("Podaj wartosc wyrazenia b:");
        b = odczyt.nextInt();

        //obliczam wyrazenie
        boolean wynik = (a >= b) && (a != b);
        System.out.println("wynik wyrazenia: " + wynik);
    }
}
