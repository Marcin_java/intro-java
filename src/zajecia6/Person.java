package zajecia6;

/**
 * Klasa Person do reprezentacji danych o osobie
 */
public class Person {
    // stan - mowi o tym jaka ta osoba jest - przechowuje jej dane
    public String name;
    public String surname;
    public int age;
    public String email;
    private boolean registered;

    // 1
    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    // 2
    public Person(String name, String surname, int age) {
        this(name, surname); // wywolanie 1
        this.age = age;
    }

    // 3
    public Person(String name, String surname, int age, String email) {
        this(name, surname, age); // wywolanie 2
        this.email = email;
    }

    public boolean pobierzWartosc() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    // zachowanie - jakas czynnosc
    public void sayHello() {
        System.out.println("Jestem: " + name + " " + surname);
    }
}
