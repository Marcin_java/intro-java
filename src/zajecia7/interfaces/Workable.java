package zajecia7.interfaces;

public interface Workable {
    void work();

    double getSalary();
}
