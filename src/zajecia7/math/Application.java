package zajecia7.math;

public class Application {
    public static void main(String[] args) {

        Matrix first = new Matrix(4, 3);
        first.fillWithRandomValues();
//        first.print();
        System.out.println();

        Matrix second = new Matrix(3, 3);
        second.fillWithRandomValues();
//        second.print();
        System.out.println("Result: ");

        try {
            Matrix result = first.addMatrix(second);
            System.out.println(result);
            System.out.println(result.toString());
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }


    }
}
